export interface Student {
    id:string,
    psyco?: number,
    math?: number,
    payment?: boolean,
    prediict?:string,
    userEmail?: string | null,
    saved?:Boolean, //האם הערך נשמר בפיירבייס או לא
    result?:string //תוצאת החיזוי
}
