import { Router } from '@angular/router';
import { PredictionService } from './../prediction.service';
import { Student } from './../interfaces/student';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AuthService } from '../auth.service';
import { StudentsService } from './../students.service';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  @Input() psyco: number;
  @Input() id: string;
  @Input() math: number;
  @Input() payment: boolean = false;
  // @Input() formType: string;
  

  @Output() update = new EventEmitter<Student>();//חובה שהמשתנה אפדייט יהיה מסוג איבנטאמיטר - כדי שהבן יוכל לשדר נתונים לאב
  @Output() closeEdit = new EventEmitter<null>(); //נשים נאל כי לא מעבירים איזשהו מידע, רק מעדכנים את אלמנט האב שהאירוע התרחש
  students$; 
  students:Student[];
  userId:string; 
  buttonText:String = 'Add student'; 
  predictText:String = 'Predict'; 
  formType: string;
  err; 
  userEmail;
  prediction: Number = null;
  prediict:string = '';
  secondpre:String = '';
  isShow = false;
  StudentsService: any;
  lastDocumentArrived: any;
  firstDocumentArrived: any;
  prev_strt_at:  any[] = [];
  index=1;

  push_prev_startAt(prev_first_doc) {
     this.prev_strt_at.push(prev_first_doc);
  }
  
 
  toggleDisplay() {
    this.isShow = !this.isShow;
  }

  onSubmit(){

  }

  predict(){ //הפונקציה חוזה האם הלקוח יחזיר  או לא 
    // this.customers[index].result = 'Will difault';
    this.predictionService.predict(this.math, this.psyco, this.payment).subscribe( 
      res => {console.log(res);
        this.prediction = Number(res);
        if(res > 0.5 ){ //אם ההסתברות להחזרה גדולה מחצי הוא לא ישאר
          this.prediict = 'Will stay';
          this.secondpre = 'Will not stay';
        } else { //אם ההסתברות להחזרה קטנה מחצי הוא ישאר
          this.prediict = 'Will not stay';
          this.secondpre = 'Will stay';
        }
      } //הצבת תוצאת ההחזרה
    );  
  }

  validation(psyco, math, payment ){ //וולידציה של שנות הלימוד
    console.log("psyco ", psyco)
    console.log("math ", math)
    console.log("payment ", payment)

    if (psyco && psyco > '800' && psyco < '0'){
      this.err ='Psyco garde must be between 0-800';
    
    }
    // console.log(Boolean(!math && !(math <='100' && math >='0')))
    if (math && math > '100' && math < '0'){
      this.err ='Math grade must be between 0-100';
    }
    
    else{
      this.err ='';
    }

    
    if(!this.err)
      this.predict();
  }

  save(){
    this.studentsService.addStudent(this.userId, this.math, this.psyco, this.payment, this.userEmail, this.prediict);
    this.router.navigate(['/students']);
    this.index=this.index+1;
  }
  
  constructor(private predictionService:PredictionService, public authService:AuthService, private studentsService:StudentsService, private router:Router) { }


  ngOnInit(): void {
    this.formType == 'Add Student';
    this.buttonText = 'Save';
    this.authService.getUser().subscribe( 
      user =>{
        this.userId = user.uid; 
        this.userEmail = user.email;
        console.log(user.email)
        this.students$ = this.studentsService.getStudents(this.userId); 
    }
      )
  }

}

