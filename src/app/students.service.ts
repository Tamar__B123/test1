import { Student } from './interfaces/student';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  studentsCollection:AngularFirestoreCollection;

public getStudents(userId): Observable<any[]> { 
  this.studentsCollection = this.db.collection(`users/${userId}/students`, 
     ref => ref.limit(4));
  return this.studentsCollection.snapshotChanges();    
} 

updateRsult(userId:string, id:string,prediict:string){
  this.db.doc(`users/${userId}/students/${id}`).update(
    {
      prediict:prediict
    })
  }

updateStudent(userId:string, id:string, math:number, psyco:number, payment:boolean){
  this.db.doc(`users/${userId}/students/${id}`).update(
    {
      psyco:psyco,
      math:math,
      payment: payment,
      prediict:null
    }
  )
}

deleteStudent(userId:string, id:string){
  this.db.doc(`users/${userId}/students/${id}`).delete();
}

addStudent(userId:string, math:number, psyco:number, payment:boolean, userEmail:string, prediict:string){
  const student:Student = { id: userId, math:math, psyco:psyco, payment:payment, userEmail:userEmail, prediict:prediict}
  this.userCollection.doc(userId).collection('students').add(student);
} 

nextPage(userId,startAfter): Observable<any[]>{
  this.studentsCollection = this.db.collection(`users/${userId}students`, 
  ref => ref.limit(4)
    .startAfter(startAfter))    
  return this.studentsCollection.snapshotChanges();
}

prevPage(userId,startAt): Observable<any[]>{
  this.studentsCollection = this.db.collection(`users/${userId}students`, 
  ref => ref.limit(4)
    .startAt(startAt))    
  return this.studentsCollection.snapshotChanges();
}
 
  constructor(private db:AngularFirestore) { }
}

