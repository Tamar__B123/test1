import { StudentsService } from './../students.service';
import { Student } from './../interfaces/student';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { PredictionService } from '../prediction.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  userId;
  students=[];
  students$;
  displayedColumns: string[] = ['math', 'psyco', 'payment', 'userEmail', 'prediict', 'delete'];
  lastDocumentArrived: any;
  firstDocumentArrived: any;
  prev_strt_at:  any[] = [];

  push_prev_startAt(prev_first_doc) {
     this.prev_strt_at.push(prev_first_doc);
  }
  
  remove_last_from_start_at(){
    this.prev_strt_at.splice(this.prev_strt_at.length-1, 1);
  }
  
  get_prev_startAt(){
      return this.prev_strt_at[this.prev_strt_at.length - 1];
  }
  
  add(student:Student){
    this.studentService.addStudent(this.userId, student.math, student.psyco, student.payment, student.userEmail, student.prediict)
  }

  nextPage(){
    this.students$ = this.studentService.nextPage(this.userId,this.lastDocumentArrived); 
    this.students$.subscribe(
      docs =>{
        this.lastDocumentArrived = docs[docs.length-1].payload.doc; 
        this.firstDocumentArrived = docs[0].payload.doc;
        this.push_prev_startAt(this.firstDocumentArrived);

        this.students = [];
        for(let document of docs){
          const student:Student = document.payload.doc.data();
          student.id = document.payload.doc.id; 
          this.students.push(student); 
        }
      }
    )     
  }

  prevPage(){
    this.remove_last_from_start_at()
    this.students$ = this.studentService.prevPage(this.userId,this.get_prev_startAt());
    this.students$.subscribe(docs => {   
      this.lastDocumentArrived = docs[docs.length-1].payload.doc; 
      this.firstDocumentArrived = docs[0].payload.doc;
      this.students = [];
        for (let document of docs) {
          const student:Student = document.payload.doc.data();
          student.id = document.payload.doc.id;
          this.students.push(student);
      }
    });
  }

  deleteStudent(index){
    let id = this.students[index].id;
    this.studentService.deleteStudent(this.userId,id); 
  }

 
  constructor(private studentService:StudentsService,
    public authService:AuthService, private predictionService:PredictionService) { }

    ngOnInit(): void {
      this.authService.getUser().subscribe(
        user => {
          this.userId = user.uid;
          console.log(this.userId); 
          this.students$ = this.studentService.getStudents(this.userId); 
          this.students$.subscribe(
            docs =>{
              console.log('init worked');
              this.lastDocumentArrived = docs[docs.length-1].payload.doc;
              this.firstDocumentArrived = docs[0].payload.doc;
              this.push_prev_startAt(this.firstDocumentArrived);             
              this.students = [];
              for(let document of docs){
                const student:Student = document.payload.doc.data();
                student.id = document.payload.doc.id; 
                this.students.push(student); 
              }
            }
          ) 
        }
      )
  
    }
  


}
